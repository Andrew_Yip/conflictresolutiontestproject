
/*
This code expect a couchDB to be running at "localhost:5984" and there to be a database called "test"
It pretty simple to change the code to get it to work with other databases.



*/

import React, { Component } from 'react';
import ReactDom from 'react-dom';
import Popup from 'react-popup';

import logo from './logo.svg';
import './App.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

var PouchDB = require('pouchdb-browser');

var nano = require('nano')('http://localhost:5984');

var PouchDBOptions = {
                    ajax: {
                      cache: false,
                      timeout: 10000,
                    },
                    auth: {
                      username: 'ja',
                      password: 'jeforz'
                    }
                  };

class App extends Component {
 
 constructor(props) {
    super(props);
    this.state = {
      CouchDB: null,
      CouchDBValue : [],
      CouchDB_Rev : "",

      PouchDB: PouchDB.default('http://localhost:5984/test', PouchDBOptions),
      PouchDB_Rev: "",
      IsPouchDBReady: false,
      PouchDBData: null,

      LocalPouchDB: PouchDB.default('localDB'),
      IsLocalPouchDBReady: false,
      LocalPouchDBData: null,

      Conflicts: []
    }
  }

  pouchDBDataOnRead = (i_pDocList) => {
    
    this.setState({
          IsPouchDBReady : true,
          PouchDBData: i_pDocList.rows
        });
  }

  setLocalPouchData = (body) => {

    this.setState({
        IsLocalPouchDBReady : true,
        LocalPouchDBData: body.rows
      });
  }

  couchDBOnRead = (err, body, header) => {
      if(!err) {
        this.setState( {
          CouchDBValue: body.rows
        });
      }
  }

  couchDBCheck = (err, body, header, i_pCouchDB) => {

    if(!err
        && this.state.CouchDB_Rev != body.update_seq) {

        this.setState({

          CouchDB: i_pCouchDB,
          CouchDB_Rev: body.update_seq,
          IsPouchDBReady: false,
          IsLocalPouchDBReady: false,
        });
        
        var l_pCouchDB = nano.db.use('test');
        l_pCouchDB.list({include_docs: true, conflicts: true, descending:true}, this.couchDBOnRead);
    }
  }

  displayName = (i_pDataField, i_pCell) => { 
    return <p>{i_pCell.doc.name}</p>;
  }

  displayValue = (i_pDataField, i_pCell) => { 
    return <p>{i_pCell.doc.value}</p>;
  }

  displayRev = (i_pDataField, i_pCell) => { 
    return <p>{i_pCell.value.rev}</p>;
  }

  displayConflict = (i_pDataField, i_pCell) => {
    return <p>{i_pCell.doc._conflicts ? i_pCell.doc._conflicts.length : 0}</p>;
  }

  sessionListOnSelect = (row, isSelected, e) => {

    var l_bSelectedRow = null;

    if (isSelected) {
      l_bSelectedRow = row;
    }
  };

  updateLocalPouch = (i_pRow, i_sCellName, i_sCellValue) => {
    let l_pLocalPouchDB = this.state.LocalPouchDB;
    let l_sColumn = i_sCellName.substring(i_sCellName.indexOf(".") + 1, Infinity);
    let l_pThis = this;

    l_pLocalPouchDB.get(i_pRow.id).then(//, {latest: true, conflicts: true}
      function(doc) {
        
        doc[l_sColumn] = i_sCellValue;
        l_pLocalPouchDB.put(doc);
        l_pThis.setState(
          {IsLocalPouchDBReady : false}
        )
      }
    )
  }

  selectRowProp = {
    mode: 'checkbox',
    bgColor: '#eeeeee',
    //hideSelectColumn: true,
    clickToSelect: true,
    onSelect: this.sessionListOnSelect
  };

  cellEditProp = {
    mode: 'click',
    afterSaveCell: this.updateLocalPouch
  }

  syncOnClick = () => {
    var l_pCouchDB = nano.db.use('test');

    var l_pThis = this;
    var l_pRemoteDB = this.state.PouchDB;
    var l_pLocalDB = this.state.LocalPouchDB;

    //var l_pCouchDB= this.state.CouchDB_Rev
    let l_pConflictList  = this.state.Conflicts
    let data = this.state.LocalPouchDBData;

/*
********************************SYNC*****************************/

    var l_pSync = PouchDB.default.sync(l_pLocalDB,l_pRemoteDB)
    .on('complete', function (info) {

      l_pRemoteDB.allDocs({include_docs: true, conflicts: true}).then(function(i_pDoc){

        debugger
        for(var i = 0; i < i_pDoc.rows.length; i++)
        {
          var l_pDoc = i_pDoc.rows[i].doc;
          var l_pCurrentRev = l_pDoc._rev;
          //var l_pRevisionNumber = l_pCurrentRev.substring(0, l_pCurrentRev.indexOf("-"));
            
          if( l_pDoc._conflicts
            && l_pDoc._conflicts.length > 0
            // && l_pDoc._conflicts[0].substring(0, l_pDoc._conflicts[0].indexOf("-")) == l_pRevisionNumber 
          )
          {
            l_pThis.checkConflict(l_pLocalDB, l_pThis, l_pConflictList, l_pDoc);
          }
        }

        l_pThis.setState({
            IsLocalPouchDBReady : false,
            IsPouchDBReady: false,
        })

      }).catch(function(err){
        debugger
      })
    })
    .on('error', function(err){
      debugger
    }) ;

  }
  checkConflict = (i_pLocalDB, i_pThis, i_pConflictList, i_CurrentDoc) =>
  {
    i_pLocalDB.get(i_CurrentDoc._id, {rev:  i_CurrentDoc._conflicts[0]}).then(function (i_pConflictDoc) {
      
    i_pConflictList.push({
                            currentValue : i_CurrentDoc,
                            conflictValue: i_pConflictDoc
                          });

    i_pThis.setState({Conflicts: i_pConflictList})

    }).catch(function (err) {
      // handle any errors
      debugger
    });

  }

  addLocalRow = (i_pDoc, b, c) =>{

    var l_pThis = this;
    var l_LocalDB = this.state.LocalPouchDB;

    l_LocalDB.post({
      _rev: i_pDoc["doc._rev"],
      name: i_pDoc["doc.name"],
      value: i_pDoc["doc.value"]
    }).then(function(response) {

      l_pThis.setState({
          IsLocalPouchDBReady : false,
        }
      )
    }).catch(function(err){
      debugger
    })
  }

  onDeleteRow = (i_pDocIDList, i_pDoc) => {

    var l_pThis = this;
    var l_LocalDB = this.state.LocalPouchDB;

    l_LocalDB.get(i_pDocIDList[0], {conflicts : true}).then(function(i_pDoc){

      i_pDoc._deleted = true;
      i_pDoc._deleted_conflicts = true;

      return l_LocalDB.put(i_pDoc);
    })
  }

  localTableOptions = {
    onAddRow: this.addLocalRow,
    afterDeleteRow: this.onDeleteRow,
  }

  resolveConflict = (i_pThis, i_pCurrentValue, i_pNotSelectedValue) =>
  {
    let l_pThis = i_pThis;
   
  // if(i_pCurrentValue._rev == i_pNotSelectedValue._rev)
   // {
       var l_pConflicts = l_pThis.state.Conflicts;
       var l_pPouchDB = l_pThis.state.PouchDB;

      l_pPouchDB.remove(i_pCurrentValue._id, i_pNotSelectedValue._rev)
      .then(  function(i_pDoc){l_pThis.clearConflict(l_pConflicts, l_pThis, l_pPouchDB, i_pDoc)} 
      ).catch(function (err) {
        //handle any errors
        debugger
      });
    //}
  }

  clearConflict = (i_pConflicts, i_pThis, i_pPouchDB, i_pDoc ) => {

    for(var i = 0; i < i_pConflicts.length; i++)
    {
      
        if(i_pDoc.id == i_pConflicts[i].currentValue._id)
        {
          
          i_pThis.state.LocalPouchDB.replicate.from( i_pPouchDB)
          .on('complete', function (info) {
             
            let l_pConflicts = i_pConflicts.shift();
                i_pThis.setState({
                  Conflicts : i_pConflicts,
                  IsPouchDBReady : false,
                  IsLocalPouchDBReady : false,
                });
          })
          .on('error', function (err){
            debugger
          });

          break
        }
    }

    Popup.close();
  }

  changesOnClick = () => {

    var l_RemoteDB = this.state.PouchDB;
    var l_pLocalDB = this.state.LocalPouchDB;

    l_pLocalDB.info()
    .then(function(results)
    {
      l_RemoteDB.changes( {since: results.update_seq}  )
      .then(function(changes)
      {
          debugger

      })
    })
  }

  render() {

    var l_pPouchDBValue = [];
    var l_pCouchDBValue = this.state.CouchDBValue;
    var l_pCouchDB = nano.db.use('test');

    var l_pThis = this;

    l_pCouchDB.info(
      function(err, body, header){

        l_pThis.couchDBCheck(err, body, header);
      })
    
    
    if(this.state.IsPouchDBReady == false)
    {
      if(this.state.CouchDB){
        this.state.PouchDB.replicate.from(this.state.CouchDB);
      }

      this.state.PouchDB.allDocs({include_docs: true,  conflicts: true}).then(this.pouchDBDataOnRead);
    }

    if(this.state.IsLocalPouchDBReady == false
        //&& this.state.IsPouchDBReady
        ){

      //this.state.LocalPouchDB.defaults = this.state.LocalPouchDB.default;
      //window.PouchDB = this.state.LocalPouchDB;

      //this.state.LocalPouchDB.replicate.from(this.state.PouchDB);
      this.state.LocalPouchDB.allDocs({include_docs: true, conflicts: true}).then(this.setLocalPouchData); 
    }
    else 
    {
      l_pPouchDBValue = this.state.LocalPouchDBData
    }
    

    let l_pConflicts = this.state.Conflicts;  

    if(l_pConflicts.length > 0)
    {
      
      let l_pConflict = l_pConflicts[0];
      let l_pThis = this;
      
      Popup.create({
          title: 'Conflict',
          content: 'There is a conflict for ' + l_pConflict.currentValue.name,
          className: 'alert',
          buttons: {
              left:[
                {
                  text: 'Keep current record of name:"' + l_pConflict.currentValue.name + '" value: "' + l_pConflict.currentValue.value + '"',
                  action: function()
                  {
                    l_pThis.resolveConflict(l_pThis, l_pConflict.currentValue, l_pConflict.conflictValue);
                    Popup.close();
                  }
                }
              ],
              right: [{
                text: 'Replace with conflict record of name:"' + l_pConflict.conflictValue.name + '" value: "' + l_pConflict.conflictValue.value + '"',
                action: function()
                {
                  l_pThis.resolveConflict(l_pThis, l_pConflict.currentValue, l_pConflict.currentValue);
                  Popup.close();
                }
             }]
          }
      }, true);
    }
    else{
       Popup.close();
    }

    return (
        <div className="App">
          <div className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h2>Welcome to React</h2>
          </div>
          <h3>CouchDB</h3>

          <table style={{"width":"100%"}}>
            <tbody>
              <tr>
                <td>Name</td><td>Value</td><td>id</td><td>Rev</td><td>Conflicts</td>
              </tr>
              {l_pCouchDBValue.map(function(i_pRow){
                  return( <tr key={i_pRow.id}>
                            <td>{i_pRow.doc.name}</td><td>{i_pRow.doc.value}</td><td>{i_pRow.id}</td><td>{i_pRow.doc._rev}</td><td>{i_pRow.doc._conflicts ? i_pRow.doc._conflicts.length : 0}</td>
                          </tr>)
                })}
            </tbody>
          </table>

        <h3>PouchDB</h3>

          <BootstrapTable
            tableHeaderClass='table-header'
            tableBodyClass='table-body'
            trClassName='react-table-row'

            data={l_pPouchDBValue}

            insertRow={true}
            deleteRow={true}
            selectRow={ this.selectRowProp }
            cellEdit={ this.cellEditProp }

            options={ this.localTableOptions }

            ignoreSinglePage
          >
            <TableHeaderColumn dataField={'doc.name'} dataFormat={this.displayName} dataSort={true}>Name</TableHeaderColumn>
            <TableHeaderColumn dataField='doc.value' dataFormat={this.displayValue} >Value</TableHeaderColumn>

            <TableHeaderColumn dataField='id' isKey={true}  editable={ false }>id</TableHeaderColumn>
            <TableHeaderColumn dataField='doc.rev' dataFormat={this.displayRev}  editable={ false }> rev</TableHeaderColumn>


              <TableHeaderColumn dataField='doc._conflict' dataFormat={this.displayConflict}  editable={ false }>Conflict</TableHeaderColumn>
          </BootstrapTable>      
          <div>
            <p>Press "Enter" to save</p>

            <button onClick={this.syncOnClick}>Sync</button>

            <button onClick={this.changesOnClick}>Changes</button>
 
          </div>
        </div>
    );
  }
}

export default App;
